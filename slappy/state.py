from __future__ import annotations

from abc import ABCMeta, abstractmethod
from typing import List

import click
import ldap3


class ReplState(metaclass=ABCMeta):
    # TODO track environment (i.e., variables)
    def __init__(self, **kw):
        if kw:
            raise Exception("Unexpected arguments.")

    @abstractmethod
    def cleanup(self):
        raise NotImplementedError

    @abstractmethod
    def dispatch_command(self, command_name: str, args: List[str]) -> ReplState:
        pass

    PAGE_THRESHOLD = 60

    def echo(self, message: str, **kw):
        if message.count("\n") > self.PAGE_THRESHOLD:
            click.echo_via_pager(message, **kw)
        else:
            click.echo(message, **kw)


class ConnectedState(ReplState):
    def cleanup(self):
        pass  # TODO don't we need to do stuff with the server?

    server: ldap3.Server
    connection: ldap3.Connection
    base_dn: str

    def __init__(self, ldap_uri: str, base_dn: str, **kw):
        super().__init__(**kw)
        self.server = ldap3.Server(ldap_uri, get_info=ldap3.ALL)
        self.connection = ldap3.Connection(self.server, auto_bind=True)
        self.base_dn = base_dn

    def dispatch_command(self, command_name: str, args: List[str]) -> ReplState:
        if command_name == 'show':
            has_a_arg = any("-a" in arg for arg in args)
            return self.show(show_all=has_a_arg)
        if command_name == 'list':
            has_a_arg = any("-a" in arg for arg in args)
            return self.list(show_all=has_a_arg)
        if command_name == 'cd':
            if not args:
                args = ['']
            new_base, *rest = args
            if rest:
                self.echo("need one argument!")
                return self
            return self.switch_base(*args)
        if command_name == 'pwd':
            self.echo(self.base_dn)
            return self
        self.echo("Unknown command.")  # TODO „did you mean…”
        return self

    def show(self, show_all=False) -> ConnectedState:
        success = self.connection.search(
            self.base_dn, '(objectclass=*)', search_scope=ldap3.BASE,
            get_operational_attributes=show_all
        )
        if not success:
            self.present_search_error()
            return self
        self.present_entries(self.connection.entries)
        return self

    def list(self, show_all=False) -> ConnectedState:
        success = self.connection.search(
            self.base_dn, '(objectclass=*)', search_scope=ldap3.LEVEL,
            get_operational_attributes=show_all
        )
        if not success:
            self.present_search_error()
            return self

        self.present_entries(self.connection.entries)
        return self

    def switch_base(self, new_base: str):
        # TODO validate base
        self.base_dn = new_base
        return self

    def present_search_error(self):
        self.echo("Bääääääh!")

    def present_entries(self, entries: List[ldap3.Entry]):
        output: List[str] = []
        for entry in entries:
            output.append(str(entry))
        self.echo(message="\n".join(output))
