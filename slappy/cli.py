import click
from prompt_toolkit import prompt

from .session import Session
from .__version__ import __version__


def run_repl(session: Session):
    try:
        while True:
            session.advance_state(prompt("sλappy> "))
    except EOFError:
        say_goodbye()


def say_goodbye():
    click.echo("Bye!")


def greet(ldap_uri: str):
    click.echo(f"sλappy v{__version__} says hello. Connecting to {ldap_uri}.")


@click.command()
@click.argument('ldap_uri')
def entrypoint(ldap_uri: str):
    """slappy - the ldap cli so simple that even bass players can use it."""
    # TODO ask for missing auth info and establish session
    with Session.establish(ldap_uri) as session:
        greet(ldap_uri)
        run_repl(session)
