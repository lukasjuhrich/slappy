from dataclasses import dataclass

import click

from .state import ConnectedState, ReplState


@dataclass  # type: ignore
class Session:
    state: ReplState

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.state.cleanup()

    def __init__(self, state: ReplState):
        self.state = state

    @classmethod
    def establish(cls, ldap_uri: str, base_dn=None):
        return Session(ConnectedState(ldap_uri, base_dn=base_dn or ''))

    def advance_state(self, user_input: str):
        args = user_input.strip().split(" ")
        if not args:
            click.echo("Did not type command!")
            return

        command, args = args[0].lower(), args[1:]
        self.state = self.state.dispatch_command(command, args)
